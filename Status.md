Status screen
=============
This page displays the Status and Statistics of the Habitat and all immediately relivant entities.

Range Chart
---------

> +4  
> +3 Bogey 01  
> +2  
> +1  
> 00 Bogey 02  
> -1  
> -2  
> -3 The Habitat, Counter-Habitat  
> -4  

System
------
| Tech: 3                | Environemnt: -1            | Resources: -1          |
|------------------------|----------------------------|------------------------|
| **Anarchist Habitats** | **Orbiting a Remote Star** | **Approaching type I** |


Allies
======
These are the priamary statistics of The Habitat and it's counter rotating resource asteroid, Counter-habitat.

The Habitat
-----------
Our home, a typical Celestial Lotus model Habitat with large solar arrays and spin gravity. It houses a few million Organic and several billion Sinthetic minds.  

|  Tech 3 | 23pts   | Stunts            | Aspects                      |
|---------------|---|-------------------|------------------------------|
| **V-shift :** | 0 | UC Equiped        | Spin Gravity Habitat         |
| **Beam :**    | 5 | Overwatch         | Central Data Core            |
| **Torpedo :** | 0 |                   | Solar Mirrors                |
| **EW :**      | 5 |                   | pure love between woman and machine |
| **Trade:**    | 5 |                   | Believe Magical Girls |

|               | Stress        |               | Consequences         |
|---------------|---------------|---------------|----------------------|
| **Hull :**    | **[ ]**       | **Mild:**     |                      |
| **Data :**    | **[ ][ ][ ]** | **Moderate:** |                      |
| **Heat :**    | **[ ]**       | **Severe:**   |                      |

Counter-Habitat
---------------
A resource asteroid that spins in the opposite direction the Habitat spins to provide stable attitude control, it is intended for future expansion.

|  Tech 3 | 23pts   | Stunts              | Aspects                      |
|---------------|---|---------------------|------------------------------|
| **V-shift :** | 0 | UC Equiped          | Counter Rotating             |
| **Beam :**    | 0 | Point Defense       | Resource Asteroid            |
| **Torpedo :** | 2 |                     | Militia Station              |
| **EW :**      | 0 |                     | Machine Hive                 |
| **Trade:**    | 5 |                     | Cave Goddess Syndrome        |

|               | Stress                  |               | Consequences |
|---------------|-------------------------|---------------|--------------|
| **Hull :**    | **[ ][ ][ ] [ ][ ][ ]** | **Mild:**     |              |
| **Data :**    | **[ ][ ][ ] [ ][ ][ ]** | **Moderate:** |              |
| **Heat :**    | **[ ][ ][ ] [ ][ ][ ]** | **Severe:**   |              |

Enemy
=====
These are the priamary statistics of the enemy.

Bogey 01
--------

|  Tech 2 | 17pts   | Stunts              | Aspects                      |
|---------------|---|---------------------|------------------------------|
| **V-shift :** | 3 | Slipstream Drive    | Suspicious IFF Codes         |
| **Beam :**    | 2 | Unknown Capability **!!!!** | Nonstandard missile loadout     |
| **Torpedo :** | 4 **??** | extended magazines  | "Private security company"   |
| **EW :**      | 2 | Point Defense       | Two ships this time          |
| **Trade:**    | 0 |                     | Lots of missiles             |

|            | Stress        |               | Consequences |
|------------|---------------|---------------|--------------|
| **Hull :** | **[ ][ ][ ]** | **Mild:**     | Tamako Sensei's forceful removal of violent Assholes from overly lewd parties volume 69 |
| **Data :** | **[X][X][ ]** | **Moderate:** | Sufficently warned |
| **Heat :** | **[X][X][X]** | **Severe:**   | Engine Shutdown  |

Bogey 02 **Taken out, Surrendered to inspection team.**
--------

|  Tech 2 | 17pts   | Stunts              | Aspects                      |
|---------------|---|---------------------|------------------------------|
| **V-shift :** | 2 | Slipstream Drive    | Suspicious IFF Codes         |
| **Beam :**    | 2 | Uknown Capability **!!!!**  | Suspected Berserker Hive     |
| **Torpedo :** | 0 | Interface Vehicle   | "Private security company"   |
| **EW :**      | 2 | Point Defense       | Two ships this time          |
| **Trade:**    | 3 |                     | Unknown Payload              |

|            | Stress        |               | Consequences |
|------------|---------------|---------------|--------------|
| **Hull :** | **[X][ ][ ]** | **Mild:**     | Tamako Sensei's forceful removal of violent Assholes from overly lewd parties volume 69 |
| **Data :** | **[X][X][X] X X X X** | **Moderate:** | Sucessfully Boarded |
| **Heat :** | **[ ][ ][ ]** | **Severe:**   |              |