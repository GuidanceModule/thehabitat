Useful Copypasta
================
A collection of useful copypasta to help with 4chan formatting.

Offical GM Tripcode
-------------------
````4chan
[green][b]Guidance Module[/b] !FKU8aDxeww[/green]
````

FATE aspects and stunts
-----------------------
````4chan
[i]Aspects are Italic Text[/i]

[b]Stunts are bold Text[/b]
````

FATE Cube Symbols
-----------------
````4chan
>Negitive die rolls
[b][[red]-[/red]][/b]
[b][[red]-[/red]][[red]-[/red]][/b]
[b][[red]-[/red]][[red]-[/red]][[red]-[/red]][/b]
[b][[red]-[/red]][[red]-[/red]][[red]-[/red]][[red]-[/red]][/b]

>Positive die rolls or skill ratings
[b][[green]+[/green]][/b] ([b][green]+1[/green][/b])
[b][[green]+[/green]][[green]+[/green]][/b] ([b][green]+2[/green][/b])
[b][[green]+[/green]][[green]+[/green]][[green]+[/green]][/b] ([b][green]+3[/green][/b])
[b][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][/b] ([b][green]+4[/green][/b])
[b][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][/b] ([b][green]+5[/green][/b])
[b][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][/b] ([b][green]+6[/green][/b])

>Blank dice or empty stress boxes
[b][_][/b]
[b][_][_][/b]
[b][_][_][_][/b]
[b][_][_][_][_][/b]

>Stress hits in boxes
[b][[red]X[/red]][/b]
[b][[red]X[/red]][[red]X[/red]][/b]
[b][[red]X[/red]][[red]X[/red]][[red]X[/red]][/b]

>Stress hits exceding boxes
[b][red]X[/red][/b]
[b][red]X[/red][red]X[/red][/b]
[b][red]X[/red][red]X[/red][red]X[/red][/b]

>Removing stress hits
[b][[green]-[/green]][/b]
[b][[green]-[/green]][[green]-[/green]][/b]
[b][[green]-[/green]][[green]-[/green]][[green]-[/green]][/b]

>Labels
([b][red]-4[/red][/b])
([b][red]-3[/red][/b])
([b][red]-2[/red][/b])
([b][red]-1[/red][/b])
([b]00[/b])
([b][green]+1[/green][/b])
([b][green]+2[/green][/b])
([b][green]+3[/green][/b])
([b][green]+4[/green][/b])
([b][green]+5[/green][/b])
([b][green]+6[/green][/b])
````

Space Combat Range Track
------------------------
This is the range track used for space combat**,** Commas indicate teathering or formation flight**;** semicolins indicate proximity but not formation. 

Ships at the same range band are close but not nessaraly moving at the same relitive velocity unless they are teathered or flying in formation.

Ships at 00 are both within visual range or each other and moving at the same relitive velocity.
````4chan
> +4  
> +3 [b][i][red]Enemy 01 ; Enemy 02 ;UsefulCopypasta.md edited online with Bitbucket Enemy 03[/red][/i][/b]  
> +2  
> +1  
> 00 [b][i][blue]The Habitat , Counter-Habitat[/blue][/i][/b] 
> -1  
> -2   
> -3 [b][i][green]Ally[/green][/i][/b]
> -4  
````