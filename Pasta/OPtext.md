OP Text Copy Pasta
==================
This is the Copypasta that will start the first thread. This document will be updated with new threads.

Habitat Quest 00
----------------

````4chan
Greetings to all Organic and Synthetic Intelligences subscribed to [i]The Habitat[/i] Volunteer Militia Mailing List. I am the [green][b]Guidance Module[/b] !FKU8aDxeww[/green] responsible for keeping this Habitat from Crashing.

I apologize for dragging you out of whatever simulated adventure narrative or drug infused orgy you were previously experiencing, but I believe a situation is developing in base reality. For those of you who lack physical bodies, I assure you that such a place still exists, that it is ruled by [i]something called Entropy[/i], and that all of your simulated environments are still dependent on the existence of physical hardware located in [i]A place where Matter Matters.[/i]

For those of you who have forgotten an outside world even exists, our Habitat is a Celestial Lotus type, consisting of a Bernal Sphere and [i]solar mirror array,[/i] spun for gravity and tensegrity, and counter rotating against a [i]resource asteroid[/i] which is intended as raw mass for further expansion. The Bernal Sphere is home to several thousand (mostly female) meat humans and other organic life forms, while the [i]Datacore[/i] at the center of the sphere is home to several billion synthetic intelligences and uploaded meatbrains. [i]The habitat[/i] and [i]counter-habitat[/i] also serves as a temporary shelter for several thousand space capable semi-autonomous mechanical platforms which preform basic maintenance and construction.

This pair of counter rotating objects is orbiting around a [i]Remote Star[/i] along with several other [i]anarchist habitats[/i] and a single planetary civilization [i]approaching type I on the Kardashev scale[/i]. Most of your ancestors migrated to this region of space to get away from Moralizing bio-conservative planetary civilizations that would "ban [i]Pure Love between Woman and Machine[/i]" according to the words of one of our habitats founders. May her source be archived in peace. This [i]freedom comes at the price of security[/i] however, and from time to time there are rumors of [i]Berserker activity[/i] in this region of space. 

I am here to notify you that there might be [i]Berserker activity[/i] in this very solar system right now.

````

Habitat Quest 01
----------------

````4chan
Greetings to all Organic and Synthetic Intelligences subscribed to [i]The Habitat[/i] Volunteer Militia Mailing List. I am the [green][b]Guidance Module[/b] !FKU8aDxeww[/green] responsible for keeping this Habitat from Crashing.

For those who have just wandered into this thread, are subscribing from one of the other local [i]anarchist habitats[/i], or the nearby planetary Civilization on [i]Midori[/i], yes the rumors are true. Aggressively self replicating machine life-forms, commonly known as Berserkers, appear to have Penitrated out system twice now. We were able to stop the last intrusion, but now they are attacking with two ships at once. The enemy appears to be trying to disguise itself as a human crewed vessel, but don't be fooled, any humans onboard are either meat puppets or edited video footage. The enemy is refusing to follow proper space traffic control procedure and is attempting to Penitrate Midori's airspace with an [i]unknown payload[/i]. MOLOCH, the local planetary administration synth, has tasked is with intercepting both vessels.

For those who can't parse 4d space/time vector markup, here is a range chart showing the relitive positions of us and both Bogies.

> +4  
> +3 [b][i][red]Bogey 01[/red][/i][/b] (Harrassing us with docking requests)  
> +2  
> +1  
> 00 [b][i][red]Bogey 02[/red][/i][/b] (Attempting to penitrate Midori Airspace)
> -1  
> -2   
> -3 [b][i][blue]The Habitat , Counter-Habitat[/blue][/i][/b]
> -4  

>Previous thread: https://boards.4chan.org/qst/thread/1979967
>Git repository: https://bitbucket.org/GuidanceModule/thehabitat/src
````

````4chan
Here is our status.

[B]The Habitat[/B]
>Beam: [b][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][/b] ([b][green]+5[/green][/b])
>EW: [b][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][/b] ([b][green]+5[/green][/b])
>Trade: [b][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][/b] ([b][green]+5[/green][/b])
>HULL [b][_][/b]
>DATA [b][_][_][_][/b]
>HEAT [b][_][/b]
Stunts,
>UC Equiped
>Overwatch
Aspects,
>Spin Gravity Habitat
>Central Data Core
>Solar Mirrors
>Pure love between woman and machine
>Believe in Magical Girls
Conciquences,
>N/A

[B]Counter Habitat[/B]
>Torpedo: [b][[green]+[/green]][[green]+[/green]][/b] ([b][green]+2[/green][/b])
>Trade: [b][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][[green]+[/green]][/b] ([b][green]+5[/green][/b])
>HULL: [b][_][_][_][/b] [b][_][_][_][/b]
>DATA: [b][_][_][_][/b] [b][_][_][_][/b]
>HEAT: [b][_][_][_][/b] [b][_][_][_][/b]
Stunts,
>UC Equiped
>One free passive defense
Aspects,
>Counter Rotating
>Resource Asteroid
>Militia Station
>Machine Hive
>Cave Goddess Syndrome
Conciquences,
>N/A

````