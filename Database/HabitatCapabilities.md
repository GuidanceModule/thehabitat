The Habitat's capabilities
==========================
An overview of the Habitat's basic abilities and non abilities in it's default configuration as a Celestial Lotus type.

Habitat weapons
---------------
We are not quite a Type I civilization yet, but we do have immense amounts of solar energy at our disposal. More than enough to anything the size of that ship to dust traveling at solar escape velocities out of the system if it gets too close to us. This is necessary for basic debris protection because otherwise the Habitat is immobile in it's current configuration. Our laser array is also capable of accelerating smaller craft, but not up to relativistic velocities on any reasonable timescale.

I don't normally advertise this to people who aren't on this Mailing List, but we also posses an information warfare capacity that would be considered military grade by a less advanced planetary civilization. Said hardware is in fact not intended for offensive use, but rather normal communications gear for a space habitat of our size. Don't go telling the wrong people about this though or they'll get worried.

More advanced offensive capabilities would require a partial reconfiguration of this Habitat. We could for instance turn our counter-habitat resource asteroid into missiles or a warship. If things get really bad we can also ask either the local Planetary Civilization or other Anarchist Habs for help. And if things get REALLY REALLY bad, the Elder races might deploy some of their advanced warships if they happen to be in the area. They seem to have a knack for that. Very suspicious if you ask me.

Hiding the Habitat
------------------
There's no stealth in space. Our habitat is made out of shiny mirrors and antennas controlled by easily excited children. Even if we could order everyone to shut up, we wouldn't be able to hide our heat signature or reduce the amount of sunlight we reflect in a reasonable amount of time. If you want stealth, I recommend burrowing a compressed archive of our Data Core and a universal Constructor inside of our asteroid, and then Jettisoning it. But it's much too late for that. The Bogey has already detected us as the closest machine made object and is focusing it's broadcasts on us.


Moving the Habitat
------------------

Building a new set of engines to transport The Habitat and/or Counter-Habitat to another arbitrary orbit within the same solar system would be relatively easy in terms of the matter+energy/time. Counter-Habitat still has massive water Ice deposits which would give us an excellent amount of Delta-V. The only downside of this plan is that it would take time to build a new set of engines, and even more time to move the entire habitat. Neither of these things can be completed while a possible enemy is lurking above us.