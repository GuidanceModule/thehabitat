A Hitchhiker's Guide to The Habitat
===================================
![Tamako Sensei](https://i.imgur.com/FvPRZQw.png)

Are you lost? It's okay if you are, we get visitors all the time. 
I'm Tamako Sensei, one of the teachers at the *St Kaname girl's accadamy and cultural exchange center*.
Part of my job involves explaining *The Habitat* and it's (lack of) rules to people like you, even if you are just passing by. 
Sometimes people have a hard time ajusting to an anarchist habitat full of *Magical Girls* and other *E-types*.
If you have any questions, don't be afraid to ask.
I garantee you I've heard it before.
And no I'm not mad at you, this is just what my face looks like.

History
-------
![Based on real historical events!](https://i.imgur.com/XwoD6UB.jpg)

This Habitat is simply called The Habitat, because it was the first one in this system. It's offial Registation is *Fully Automated Luxury Gay Space Communist Habitat 00*, but nobody calls it that because it's long and undignafied.
You build the first of something in the system, you earn the right to give it whatever short name you want. 
Hence why most stars are simply called "Sun" and most planets are simply called "Dirt" by the locals.

*The Habitat* was founded roughly 200 (local solar) years ago by sexual, technological, and political refugees from a planet called Dirt orbiting a star called Sun. (Probably not the ones you're thinkng of)
I'd be revealing my age if I told you I was there or not, but I can tell you Dirt was at the time ruled by a Localist, Imperalist, Cis, Heteronormative, Capitalist, Patrarchy.
Our founders (one of which I may or may not be) were, to put it mildly, a mechaniophile, lesbian, anarchist, commune, with open source theroy, and open standard praxis.
They escaped their dying world (no really, it had a neuclear war just a few decades latter) by pooling together money made by selling Dojins, and bought a UC and passage offworld from Rogue Traders.
After many adventures, most of which are Adult rated, but which you can read about nevertheless in our free manga library, we came to this system (uninhabited at the time) and built *The Habitat*.

Today, *The Habitat* is the system's largest communications hub and a popular destination for virtual tourism.
It supports a post scarcity economy based on Nanotechnology and the export of audiovisual media.
HA! That's a good one; We mostly just sell porn.
The other things are true, but pornography is pretty much the only thing we even bother selling anymore, and we don't have many imports either.
Our *Massive... economy... the biggest in the solar system.*
Bigger than the local Planetary Civilization and every other habitat combined.
Go ahead, you can laugh. Okay, that's enough laughing. Penis jokes aren't that funny.

While The Habitat was mostly founded by lesbians with a tallent for visual arts and information sciences, (of which I count myself) all forms of Organic and Synthetic life are free to visit if they agree to respect our (lack of) rules.
If you want to Immagrate, you'll have to wait in line. 
This is a closed ecosystem populated mostly by immortal lesbians with access to in vitro fertilization.
It's not a race or gender thing, we just don't have room.
Have you tried the other *Anarchist Habitats*?
You know we could give you a Universal Constructor (or the plans for one) if you agree to follow our TOS and COC.

Geography
=========
The Habitat is a Celestial Lotus type, consisting of a Sphere shaped spin gravity habitat (what most people mean when they say The Habitat), a large array of mirrors, solar pannels, antennas, and radators, and counter rotating resource asteroid creatively named Counter-Habitat.
The Data Core at the center of the Sphere contains too many simulated virtual environments to count, but I'm only going to talk about the physical space on the inner surface of the sphere where I live.

The inner equator of the sphere has a comfortable amount of artifical *spin gravity* that gradualy tapers off to nothing as you approach the poles.
Along the very equator is a long circular system of *rivers and lakes* that are used for transportation, recreation, and water filtration system.
On one side of the habitat, the rivers drain into a salt water lake with *diamond sand beaches* that we jokingly refer to as *The Sea* dispite being objectively kind of small compared to the lakes on most planets.
Under the sea is our *water filtration plant*, which distributes fresh water to the entire habitat. On *an island* in the middle of *the sea* is *The Castle* where all our princesses are born and raised until they reach adulthood.
The Castle is staffed by a *team of dedicated maids*. 
Adult guests may visit the castle, but only if they consent to following *very strict rules* about proper behavior.

On both sides of the Sea are *rural areas* where most of the Habitat's food is grown using *very efficent agriculture methods*. 
These *rural areas* gradualy fade to *suburban zones* and eventualy *urban areas* until they culminate in our famous *Red Light District,* located antipoidal to *The Castle*.
Located halfway between *the Castle* and the *Red Light District* on either side at the boundry between the *Suburban Zones* and Rural areas, are the *Medical Center* and *St Kaname girl's accadamy and cultural exchange center.*

Light enters the habitat through large shuttered windows located near the poles. Because the habitat is not big enough to have a realistic day and night cycle, these simply open and close at regular intervals to simulate day and night on an arbitrary schedule based on the lenght of the days on the nearest planet.
In the center of The Habitat is *The Axis* an area with zero gravity around which the entire habitat revolves. *The Axis* contains the *Datacore*, the *Universal Constructor*, and *Grand Central Station,* the main starport and train station. 
Transportation around the Habitat mostly happens on foot, but there are also *various watercraft*, *flying platforms,* and a mag-lev train system with 6 stations.
The stations are located in the *Red Light District*, *Medical Center*, *Castle*, *School*, *Grand Central Station* in *The Axis,* and *Turn-around* in Counter-Habitat.

#### Turn around
Turn around is the shortest line, so named because the single train has to match rotation with counter-habitat before stopping.
It only makes one stop in *Counter-Habitat* before going back to *Grand Central Station* while most of the other trains make three or four stops on their route.

#### Galaxy Line
The longest line, traveling along the outside of the habiat and stopping at the four equatorial stations.
Because it travels counter rotating against the Habitat's spin, passengers are weightless while in transit, making it popular for sightseeing.
The Galaxy line has the most cars because they all travel on the same track in the same direction while all other lines are bi-directonal with either one car on one track, or multiple cars on double tracks.

#### Gravity Line
Existing mostly to compensate for the temporary loss of angular momentem that would be caused by Galaxy line.
Gravity line runs paralell to Galaxy line, and moves whenever Galaxy line does. Gravity line only has four cars, but the cars it does have are used for either raw materials storage, or high gravity recreatonal facilities.
Many meat based citizens enjoy exercizing in high gravity, and the cars are often turned into fight clubs, bars, or gyms for people who can handle it.

#### Sky Line
The only line that maintains a more or less constant gravity through it's entire trip, and also the slowest line. 
It travels between the *School*, *Red Light District*, *Medical Center*, and back to the *Red Light District*, before returning to the *School*.
There is some debate as to whethere or not a train that regularly stops in the *Red Light District* should have an **A** rating or not, but most agree that the district's train station and streets are public spaces and carry an **M** rating. 

#### Axis Line
A line that travels between the *School*, *Grand Central Station*, *Medical Center*, before returning to *Grand Central Station* and back to the *School.* People wishing to avoid the *Red Light District* usualy take this train even if it means more walking.

#### Royal Carrage
The only train with an **S** rating that children are allowed to ride without parental supervision. 
It normaly only goes too and from *The Castle* and *Grand Central Station*, but it also sometimes travels on other tracks for special events. 
It is highly decorated.

#### Party line
This and Gravity Line are the only trains with an **A** rating rather than an **M** rating.
Normaly it goes from *The Red Light District* to and from *Grand Central Station*, but it also sometimes circulates on the outside line for extended periods of time while hosting orgies in zero-gravity.
During this period it only stops periodicly in the Red light District every few hours.

#### Emergency vehicles
Several Emergency vehicles are also designed to make use of the train tracks. 
The largest are specificly designed mobile hospitals stored in the *Medical Center* when not in use.
In an emergeny they can be deployed to almost any location in the habitat in under 60 seconds, after which they deploy flying platforms to rescue sick or wounded individuals.
Addiotnaly all the train cars are designed to operate in a vaccum, and can be used as emergency escape pods if jetisoned.

#### Other vehicles
There are no privately owned motor vechiles in *The Habitat* only semi-autonamous groundcars such as *Limos and the occasonal Delivery Van.*
There are many watercraft, ranging from *Semi-autonamous water taxies* to *non powered sailing vessels*, and the famous *Party Barges* which can be booked for private events.
Another common sight inside the Habitat are *Flying Platfroms.*
These are twin drive ducted fan craft capable of lifting 100kg in the Habitat's low gravity.
Larger loads are carried by multiple Platforms in tandem.
Mecha and Humanoids under 100kg can wear them like a backpack and be carried around the habitat if you don't want to use other forms of transportation.
Normaly *Flying Platforms* are semi-autonamous, but if you have proper training from the Militia, you can directly control one, and doing so is considered a prequesite for piloting most spacecraft.
Some people even stand on the platforms and try to guide them by shifting their body, but this is considered unsafe and not recomended unless you have backups.
Just make sure you don't crush anybody else when you fall off.

Outside the Habitat
===================
According to the Human Diaspora system classification, our solar system looks like this.

| Tech: 1-3              | Environemnt: -1            | Resources: 1           |
|------------------------|----------------------------|------------------------|
| **Anarchist Habitats** | **Orbiting a Remote Star** | **Approaching type I** |

We were the first spacefaring civilization to perminantly inhabit this system in over 10,000 years.
Asside from a single habitable planet named *Midori* (Green in a dead language) with the ruins of long post-collapse technological civilization, it was completely deserted.
Of course things always change.
Our habitat quickly spawned several other *Anarchist habitats* ultimately re-opening contact with the local Planetary Civilization on "Green"

![Naga Ruins](https://static9.depositphotos.com/1056393/1075/i/950/depositphotos_10756803-stock-photo-ancient-stone-sculpture-in-angkor.jpg)

10,000 years ago, *Midori* belonged to the *Naga*, a race of cybernetic serpent/dragon things which interbread with humans at one point to produce the locals.
At some point their civilization collapsed for unknown reasons leaving behind ruins on many planets across this sector of space.
Our *archaeology club* has permission from the *Naga Relicary* civilization to investigate the ruins and attempt to re-contact any surviving Naga who still posess their mental faculaties, but so far most of the ones we've encountered seem to be senile or insane. 
Most of the planet's current inhabitants consider the *Naga* to be either an old supersticion, or otherwise irrelivant to modern life, and instead persue either a less extreme version of our libritarian lifestyles, or a violent reaction to what they percieve as the same "degeneracy" that doomed their ancestors.
The Naga's fondness for carving sculptures of their human concubines is considered a reflection of our society's so called "Sexual Degeneracy."

Today *Midori* has reached Tier I of nano-technological development and is administrated by *MOLOCH* a machine inteligence bassed off of a neutered version of the Habitat GuidenceModules.
The locals don't trust Synthetic Inteligences fully, so *MOLOCH* is only Semi-Autonoamous, deligating most of it's desision making to meat based organic life forms.
*Moloch* claims however that it's capabilities are just as sophisticated as any in the System, but that it's only willingly limiting itself for the sake of it's human Citizens.

*Midori* has one moon, named *Aoi* and other than that there are only three other planets in our solar system. 
A small red planet jokingly named *Rogue* orbiting closer to the sun, and two Gas giants named *Jotton and Titan* in the outer solar system.
All these planets have moons of their own, but they are too small and unimportant to name, so most just have numbers, but many have unoficial names taken from classic movies and anime.
None of these moons are perminantly inhabited, and very few of them are even explored. Most organics live on *Midori* and most synthetics live on sun orbiting habitats made out of random asteroids that occasonaly clutter the inner system.

According to *The Galactic Council,* we have *Elder Race* status over *Moloch* and that our combined meta-civilization is *Approaching type I on the Kardashev Scale.* 
They also claim that the *Naga Relicary* are our *Elder Race* and that the nearest celestial authority is a type II civilization known only as *The Dowager sun* located several slipspace jumps away from us. 
There are a few other planetary civilizations in the cluster, including a planet named *Gaia* who's monarchy recently discovered orbital spaceflight with the help of an Elder Race known as the *Sensai*
Aasside from that there isn't much going on in this volume of space, sparing the occasonal *Pirate raid* or *Bersker outbreak*.

Politics
========
The Habitat was officaly founded under the Idological precepts of "Fully Automated Luxury Gay Space Communism" but has since decided to re-categorize itself after disagreements over what "fully automated" and "gay" meant.
Current (non party) doctrine is that "Full Automation" is a violation of individual freedom, and that "Gay" is Cisnormative. 
Because nobody can agree what "Comunist" means, and "Space" can mean anything, "Anarchist" or "Other Libritarian" is the perfered idological descriptor.
If you think the 2d political compass used by some primative cultures is a useful abstraction, we exist somewhere on the bottom left corner.

As it stands, we try to maximize individual freedom while sharing as much public property as possible and minimizing authority whenever sensible. 
You aren't allowed to hurt people without their permission, or do anything that might damage the habitat's vital infistructure.
Other than that you can do whatever the fuck you want, as long as you are okay with the same thing happening to you.
I'm serious about the last part, there are a lot of people just looking for a socialy acceptable excuse to commit horrible violence against you.

On Weapons and public safety
----------------------------
![The Volunteer Militia](https://i.imgur.com/7eRdUWc.jpg)

Public safety in and around the Habitat is the responsibility of the Habitat's own Volunteer Militia (of which I am affileated with but no longer a part of).
They wear no badges, but can be identified by the fact that they are the only ones allowed to carry "dangerous weapons" inside The Habitat's preasurized volumes.
Unfortunately for your ability to recognize them, what does and does not count as a weapon inside The Habitat is often counter intuitive.

The current definition is any device capable of projecting a beam or solid projectile capable of puncturing the habitat, any blade capable of doing the same, or any chemical explosive above a specific potency.
This excludes.

* Allmost all melee weapons under 3m long
* Most bows, airsoft guns, lasers, and firearms, with the exception of most modern liniar rifles.

This does not however exclude most firearm cartrages.
You can have all the guns you want, but you can't have any bullets.
Nobody will even look twice if you walk around with a sword, bow or *magic wand* (mine shoots lasers and delivers electrical shocks).
We even have a small airsoft community, but they usualy meet in virtual reality because they got tired of being mistaken for Milita volunteers.

The militia is a non hierarchical organization which answers directly to The Habitat's Guidence Module, the closest thing we have to a real leader.
In times of emergency they can assome authoritarian control over the entire habitat and all it's inhabitants, but that's only done in times of extreme urgency.
These emergencies are demarkated by the Guidence Module calling one of the many color codes.

* Code Red: Attack by external enemies
* Code Orgage: Fire or environmental hazard
* Code Yellow: General safety hazard
* Code Green: Biohazrd
* Code Blue: Civil Unarest
* Code Indigo: Mimetic Hazard or cyber intrusion.
* Code Violet: Used to stand down alerts
* Code Monochrome: Emergency spin-down procedure, also called code White or code Black.

And no, there aren't any color codes for the rest of the electromagnetic spectrum, and if they were, I probably wouldn't be able to tell you about them.
While these codes are called, the Habitat is temporaraly considered to be Authoritarian Communist until code Violet is called. 
During this period the habitat's external hull, along with the hulls of all semi-autonamous platforms being controlled by the militia, will change to the appropriate color.
Some militia volinteers chose to wear skinsuits that are also capable of changing color, but most prefer individualistic costumes to color coded uniforms.

During Code Violet (Normal) Militia Volinteers have no special authority other than the authorization to carry weapons. 
Anybody other than a Militia member brandishing a dangerous weapon, automaticly activates code yellow until the individual is dissarmed.
Any stockpile of explosives discovered activates code orange, and other hazardous materials such as chemical and biological agents have their own color codes.
Other than this there is no penalty for having a weapon, because there is no private property or penalties for having things.
Anybody may recieve Militia training and even use all the dangerous weapons they want (including small neuclear weapons) on the far side of Counter-Habitat with the proper supervision.

Standard Militia weapons varries but tends to consist of Liniar Rifles, Phased Array Lasers (PAsers for short), V-Blades, Micro-warheads. 
Humanoid militia members tend to wear Skinsuits on duty, enhanced by semi-autonamous weapon platforms called "Bits." 
Mechanical (or Mecha) militia members tend to have spider-like or insectoid morphologies in order to maximize mobility in 3d space in and around a spin gravity habitat.
Giant humanoid platforms are sometimes used, but not considered to be practical or elegant for most situations.

In the *Urban areas* sorounding the *Red Light District* standard Militia equipment is considred too dangerous, and public safety is the responsibility of *Lolitia Squad* a group of eccentricly dressed humanoids wielding commicly oversized weapons.
As *Lolita Squad*'s gear is not considered a *Dangerous Weapon* they are not required to be Militia members, but are expected to coperate with the *Volinteer Militia* and *Guidence Module* in a non hiearachical manner.
Most of the time *Lolita Squad*'s job consists of *forcibly removing assholes* (and/from) unruly guests at parties and supervising BDSM play.
I volinteer with them as part of my night job, and no *this body dosn't need to sleep*.
Having two jobs in a post scarcity economy is a sign of high status not poverty.
It's a bit like being a superhero except my identity isn't really a secret.

![Lolitia Squad](https://i.imgur.com/YPidMEi.png)

On free speech and intelectual property
---------------------------------------
![Home Taping](https://upload.wikimedia.org/wikipedia/en/b/bc/Home_taping_is_killing_music.png)

Ahahahahahahahahahahahahahahaha!

You're kidding right?

Information can't be owned, and speech is free unless it's the kind of speach that hurts people like viruses and slander.

* Spreading viruses will activate a code Indigo or code Green if it's the biological kind.
* Inciting a riot will activate a code blue.
* Being naked in the wrong place will activate a code pink. (Not an offial militia color code)

Saying mean or untrue things about people will cause them to politiely ask you to appologize.
If you don't appologise, they might say mean things back.
If things get really bad, the Guidence Module might step in by... making an offical statement about who was right and who was wrong.
This almost never happens.

If it isn't one of those cases I just listed, feel free to say whatever the fuck you want, and share all the information you want without paying. 
Just make sure to give people credit for things, or it counts as slander, which will result in... 
people asking for an apology and then saying mean things about you if you don't take it back.

There are however, a few terms which are considered slightly offenseive and not used in polite conversation.

* Robot: This word means slave in a dead human language. Machine or "Platform" is considered more polite.
* Automated/Autonamous/Automaton: See robot. Implies a lack of free will. Semi-Autonomous is prefered to distinguish machines with free will from machines without free will. 
* AI/Artifical Inteligence: There is nothing artificial about any form of inteligence, machine or otherwise. Synthetic Inteligence or Machine Life are prefered.

Just between you and me, it's not a good idea to insult machines when you live in a space habitat maintained by machines.
Us organic meat humans are just guests.

On wealth and private property
------------------------------
Again, do you not understand what Comune means?
We aren't the only comunists in the galaxy you know.
Space travel and nano-technology makes it even harder to justify private property.
Some habitats get super anal about who owns what cubic volume of space and which pieces of physical matter, but really it's too much trouble for us.
You own your body, and anything you regularly use.
If you want something, you can ask for it, and usualy somebody will give it to you if they aren't using it themselves.
This includes access to the Universal Constructor, and builder mechs, which will make anything you want as long as it isn't something that will get the Militia Called.
If you leave something laying around and the Cleaners find it, consider it recycled. 
You can always get another one just by asking, try not to litter next time.
If it's something with oubvious sentemental value or clearly handmade, the Cleaners are semi-autonamous and usualy avoid that, but somebody else might have taken it, try the lost and found mailing lists.

Everything else belongs to everyone else.
The only exceptions are rooms, which may be temporaraly registered to a single indiviudal or group of individuals.
Rooms in physical space are managed by the Guidence Module.
The amount of rooms you can own at a time depends on what kind of rooms they are. 
(Only one bedroom at a time, but multiple storage areas, and you need more than one person to claim a larger space for a party)
The owner of a room has the authority to evict anybody they want out of their room.
If you don't comply, the Militia might be called, and it might become a localized code blue.
If this happens too often, the Guidence Module might not allow you to enter specific rooms for any reason.
"Outdoor" areas are always public spaces and cannot be claimed, except by special permission from the Guidence Module.
The creation and destruction of empty rooms is usualy preformed by semi-autonamous Builder Machines based on specifications from the Guidence Module and individual citizens.
The purpose of a room is determined by it's current owner, but the Guidence Module is free to overide any room if it feels there is a zoning conflict.

On crime and punishment
-----------------------
There are few crimes in the Habitat, and even fewer Punishments.
Both Prisons and Judicial systems are considered unessarialy authoritarian.
Instead the Guidence Module keeps an eye on everyone and makes sure nobody gets hurt.
If somebody is being dangerous, the Militia are authorized to use any means nessary to stop it.
If you are caught in the act of violating somebody elses's rights, you are guilty of committing a crime.
If you are not caught in the act, then it's just a nasty rumor and should be treated as such.
Even if somebody is caught committing a crime, the Militia may chose not to do anything about it if they don't believe you are dangerous anymore.
The only real punishment used by the Habitat is social ostrisization and banishment.

Minor crimes are left to the community and court of public opinion.
If people don't like you, they'll say mean things about you until they get over it, or you decide to leave.
Major crimes are dealt with by banishment.
If the defendent exists in virtual space, they will have all their copies moved to a sandboxed server with the ability to transmit themselves to anywhere except the habitat itself.
Attempting to re-connect with the habitat will result in your connection being immediately terminated without warning.
If the defendent only has a physical body, they will be physicaly pushed out of an airlock. Usualy while wearing a spacesuit, and usualy into a spaceship if at all practical.
Airlock flushing is something that only really happens in emergencies, it's a huge waste of valuable resources.

On children and public decency
------------------------------
![Student Uniform](https://i.imgur.com/dFy5OYa.jpg)

Lots of people ask me, "do I wear this outfit while teaching children?"
The answer is no, I don't wear anything when I teach children, because I don't teach children. Period.

Because of the closed ecosystem and easy access to clinical immortality, there are very few Organic children in the habitat, not even enough to fill a single classroom.
By tradition, all Organic children born in the Habitat or addopted by the Habitat are given the honorary title of "princess" and raised in a castle in the area with the lowested population density until they reach maturity.
This is for their own protection, as an anarchist society cannot otherwise protect them from sexual abuse at the hands of some of our more unsavory inhabitants.
*St Kaname girl's accadamy and cultural exchange center* is technicaly a university that also offers high school level courses. 
Many of it's students chose to inhabit bodies that look like teenage girls, but all of them are considered adults or the moral equivlent of adults.
Even if they don't always act like adults, nobody really does in The Habitat.
And don't bother asking what "Adult" even means, because not everyone here is human, and our years might not even be the same as your years. 
18 (local solar) years or a comparable amount of accumulated virtual experiances is the best I can offer, and that only really applies to Humanoid Organics, which is it's own can of worms definition wise.

Dispite this, the topic of public decency still cause divisions in a lawless community founded by sexual deviants.
After a conflict which resulted in the spawning of the *Family Matters* habitat, and the *Think of the children* habitat, All visual content in the Habitat, including the physical volume of the habitat itself, is divided into three ratings.

* **S** for Safe. Content that can be viewed safely by all audiences.
* **M** for Mommies. Every child in the Habitat has an assigned Mommy allowed to make desisions on her behalf. Children may consume M rated content with their Mother's permission.
* **A** for Adults. Only adults may consume this.

The vast majority of the Habitat is rated M.
In the Habitat's physical volume, this includes almost all public spaces except for individual homes and the red light district which are rated A.
The only place that is rated S is the Castle and it's immediate soroundings, located as far away from the red light district as possible.
Under an M rating the following dress code is enforced.

* All humanoid nipples must be covered reguardless of chest size or gender identity. Yes, that last part IS nessary. If you want to go topless, you can have the nipples removed.
* The anus and all genetalia of all humanoids must be covered by a non transparent covering. It dosn't have to be very big or cover anything else, but it must be covered and it must be opaque in the visual spectrum.
* For sanatation reasons, the soles of all meat based feet should also be covered at most times in base reality, but not in virtual reality.
* 2d images may flaunt these rules as long as the nipples, anus or genetalia, are obscured to the viewer.
* All these rules may be temporaraly lifted in oubviously non sexual contexts such as swimming and bathing on a case by case basis.

Failure to observe this dress code will... result in the visual area around you temporaraly upgraded to **A** until you put on clothes or leave.
Some people call this a code pink, but that's just a joke, it's not an offical color code, and dosn't even involve the militia.
Being naked isn't a crime, although if you do make a habit of this, people will call you a pervert and you won't be invited to non lewd parties.
Rape however is a crime, and you can expect the Volinteer Militia to shoot you on sight and then exile any backups you might have if you get caught doing anything to anybody without their permission.
And no, we don't have a problem with BDSM or other instances of simulated rape, as long as no children are involved and everyone present agreed to it, you can do ANYTHING YOU WANT.
Yes, anything, I mean it.
No, I don't fuck tourists or students, right now you are both.
Go bother somebody else unless you want me to show you what this wand can do.

![Fuck off](https://i.imgur.com/uq1wdPJ.png)