README
======
Greetings to all Organic and Synthetic lifeforms. 

This is the Git repository for The Habitat, a quest thread run every tuesday at on [4chan's /qst/ board](https://boards.4chan.org/qst/).

I am the **Guidance Module !FKU8aDxeww** responsible for keeping this Habitat from crashing.

If you are reading this, you are most likely one of the Organic and Synthetic inteligences living inside the Habitat or it's many simulated environments.

The Habitat is a semi-closed ecosystem containing a semi anarchic post scarcity society.

Under normal operating conditions, each citizen is allowed to do whatever they they want as long as they don't damage or otherwise threaten the habitat itself.

Those who violate these minimal rules may be removed as threats in any manner which does not constitute authoritarian rule.

In the even the habitat is threatened by an outside force, citizens who have subscribed to the Militia mailling list will be allowed to propose and vote on appropriate responsees.

As the **Guidance Module !FKU8aDxeww** I am responsible for carrying out these sugestions to the fullest of my ability.

Information about the volume of space outside the Habitat will be represented using Diaspora's space combat rules.


*[Guidance Module !FKU8aDxeww]: The tripcode I use to sign all offical communications.

*[Organic]: A life form which evolved naturaly. This usualy refers to humanoids of mamilian descent but can refer to any form of organic life. Most Organics live in base reality, but some spend sugnificant amounts of time in simulations with their physical bodies on life support.

*[Synthetic]: A life form which was created for a specific purpose. Sometimes also called a Machine life form, Artifical Inteligence, or Robot. Most Synthetics are coded from scratch, but many include simultions of organic minds or information uploaded from an organic mind.

How does voting work
--------------------
Whenever the **Guidance Module !FKU8aDxeww** needs to make an important desision, it asks the players what to do.

Player consensus requires that a sugestion has recieved at least two votes from seperate post IDs, and more votes than any other incompatable sugestion.

Once consensus has been reached, a player who voted may roll **dice+4d3+-8** to simulate rolling the *4 FATE cubes* to make it offical if the sugestion required the roll.

If the sugestion did not require a roll, the **Guidance Module !FKU8aDxeww** is free to end voting whenever they are satisfied with the players sugestion.

The **Guidance Module !FKU8aDxeww** will also make defensive rolls automaticly, and roll for the enemy.

Any time the dice are being rolled, a single player may sugest an *aspect* (Almost always represented by *Italicized* text) be invoked, offering a fate point in exchange for a reroll or +2 to a previous roll.

When this happens, the gods may accept or refuse this invocation depending on how appropriate it is.

Players and the **Guidance Module !FKU8aDxeww** may also offer compells, forcing the players or enemy to either preform a specific action (or inaction) in exchange for a fate point, or give a fate point to refuse.

When the players are offered a compell, they may vote on wether to accept it or not using the regular rules.

When the **Guidance Module !FKU8aDxeww** is offered a compell, they may chose to accept it (taking a fate point), refuse if (giving a fate point), or ignore it completely if it is inapropriate.

Inapropriate or nonsensical sugestions of any kind may be disreguarded for any reason.

*[dice+4d3+-8]: The 4chan dice notation used to roll 4dF required by the game's mechanics.

*[4 FATE cubes]: in universe name for Fudge dice. Also written as 4dF.

What's Diaspora?
----------------
Diaspora is a FATE based roleplaying game by [VSCA Publishing](http://www.vsca.ca/Diaspora/).
You can read the freely avalable SRD on their website [here](http://www.vsca.ca/Diaspora/diaspora-srd.html).

Why a Git repository?
---------------------
Normaly quest threads copy paste a bunch of text including archive links and the answers to common questions into the OP of each new thread. 
Sometimes this information gets really long and needs to be put in a pastebin. 
I chose a Git repository instead for the reasons listed below.

 * I like markdown and Pastebin does not handle markdown very well
 * Git lets me store all the information in one place, back that one place up in many places, and keep track of changes so I can fix mistakes
 * Open source software like git is in tune with the ideals of the Habitat
 * I might even be able to archive the threads themselves on git, making the quest itself open source software

You can read all about the Git version controll system on [Wikipedia](https://en.wikipedia.org/wiki/Git).

What's a quest thread?
----------------------
According to [4chan's /qst/ board][], a quest is defined as a form of "author-driven collaborative storytelling" but you probably already knew that.

[4chan's /qst/ board]:https://boards.4chan.org/qst/

Who writes this shit?
---------------------
The author of this quest goes by [@PomoWarfare][] on twitter Be warned, they frequently post spicy political takes. 
They've also run a few shitty quests back on /tg/ and anonkun that they hope you don't remember. 

The first quest they ever ran was called [Space Princess Quest][] and it takes place in the same universe as this one, but at a much lower tech level. 
Had SPQ not fallen appart, the protaganist would have ended up somewhere resembling the Habitat. Maybe her planet will show up in this quest?

[Space Princess Quest]:http://suptg.thisisnotatrueending.com/archive.html?tags=Space%20princess%20quest
[@PomoWarfare]:https://twitter.com/PomoWarfare

